#! /opt/local/bin/python3
# -*- coding: utf-8 -*-

import os, sys
from FolderWatch import FolderWatch
from ImageController import ImageController
from View import View
from PyQt5.QtCore import Qt, QTimer

workSpace = "/Volumes/Workspace"

def update():
    fileList = FolderWatch.getFileList(path = view.getWorkSpace())
    view.showList(fileList)

if __name__ == '__main__':
    sys.argv.append("--disable-web-security")

    app = View.createApp(sys.argv)

    view = View(os.path.dirname(os.path.abspath(__file__)), workSpace)

    fileList = FolderWatch.getFileList(path = view.getWorkSpace())

    timer = QTimer()
    timer.timeout.connect(update)
    timer.start(2000) # [ms]

    view.show()

    sys.exit(app.exec_())
