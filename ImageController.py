from PIL import Image, ImageQt
from io import BytesIO

import re, subprocess

class Foundation:
    @staticmethod
    def run(cmd):
        return subprocess.run(cmd, stdout = subprocess.PIPE).stdout

class ImageController(Foundation):
    @staticmethod
    def __convertImageQt(data, imageType = "RGBA"):
        image_PIL = Image.open(data)
        image_PIL = image_PIL.resize((268, 178))
        image_PIL_RGBA = image_PIL.convert(imageType)
        return ImageQt.ImageQt(image_PIL_RGBA)

    @staticmethod
    def readThumbnail(file):
        if re.search(".(?i:jpg)$", file) is not None:
            data = Foundation.run(["exiftool", "-ThumbnailImage", "-b", file])
        elif re.search(".(?i:[dng|pef])$", file) is not None:
            data = Foundation.run(["exiftool", "-PreviewImage", "-b", file])
        else:
            return

        if len(data) == 0:
            return ImageController.__convertImageQt("nopreview.png")
        else:
            return ImageController.__convertImageQt(BytesIO(data))

    @staticmethod
    def readGPS(file):
        data = subprocess.run(["exiftool", "-gpsposition", "-T", file], encoding='utf-8', stdout = subprocess.PIPE).stdout.rstrip("\n")

        return data
