from ImageController import ImageController
from ImageController import Foundation
from PyQt5 import (QtWidgets, QtWebEngineWidgets, QtGui, QtCore)
import os, re

class Filter(QtCore.QObject):
    def eventFilter(self, widget, event):
        if event.type() == QtCore.QEvent.KeyRelease:
            if event.key() == QtCore.Qt.Key_Space:
                widget.preprocessImplantGPSTag()
                return True

        return False

class ImageLavel(QtWidgets.QLabel):
    def showImage(self, imageqt):
        pixmap = QtGui.QPixmap.fromImage(imageqt)
        self.setPixmap(pixmap)

class WorkSpaceLabel(QtWidgets.QLineEdit):
    def __init__(self, workSpacePath):
        super().__init__()
        self.setWorkSpace(workSpacePath)

    def setWorkSpace(self, workSpacePath):
        self.setText(workSpacePath)

    def getWorkSpace(self):
        return self.text()

class ListWidget(QtWidgets.QListWidget):
    def __init__(self):
        super().__init__()
        self.__current = None
        super().setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def changeCurrent(self, current):
        self.__current = current.text()

    def getCurrent(self):
        return self.__current

class View(QtWidgets.QWidget):
    @staticmethod
    def createApp(argv):
        return QtWidgets.QApplication(argv)

    def __init__(self, execPath, workSpacePath):
        super().__init__()

        self.__selected = None

        self.__imageLabel = ImageLavel()
        self.__workSpaceLabel = WorkSpaceLabel(workSpacePath)
        self.__fileList = ListWidget()
        self.__webView = QtWebEngineWidgets.QWebEngineView()
        self.__webView.load(QtCore.QUrl("https://momozow.gitlab.io/MmzwCameraUtility/"))

        self.__layout()
        self.__setSlot()
        self.installEventFilter(Filter(self))

    def __layout(self):
        self.__fileList.resize(120, 240)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.__imageLabel)
        vbox.addWidget(self.__workSpaceLabel)
        vbox.addWidget(self.__fileList)

        hbox = QtWidgets.QHBoxLayout()
        hbox.addLayout(vbox)
        hbox.addWidget(self.__webView)

        self.setLayout(hbox)

    def __setSlot(self):
        self.__fileList.currentItemChanged.connect(self.__showThumnail)
        self.__fileList.itemDoubleClicked.connect(self.__pasteWorkspaceAndDoubleClickedItemAndSet)

    def __showThumnail(self, current, previous):
        if current is None:
            return

        self.__fileList.changeCurrent(current)

        currentPath = self.__workSpaceLabel.getWorkSpace() + "/" + current.text()

        if self.__selected != currentPath:
            self.__selected = currentPath

            thumbnail = ImageController.readThumbnail(self.__selected)

            if thumbnail is not None:
                self.__imageLabel.showImage(thumbnail)

    def __pasteWorkspaceAndDoubleClickedItemAndSet(self, item):
        if item is not None:
            workspace = re.sub(r"/\*$", '', self.__workSpaceLabel.getWorkSpace())
            folder = re.sub(r"/*$", '', item.text())
            self.__workSpaceLabel.setWorkSpace(workspace + "/" + folder)

    def getWorkSpace(self):
        return self.__workSpaceLabel.getWorkSpace()

    def showList(self, fileList):
        fileList = sorted(fileList)

        currentFilelist = []
        for i in range(self.__fileList.count()):
            currentFilelist.append(self.__fileList.item(i).text())

        if currentFilelist == fileList:
            return

        currentFileName = self.__fileList.getCurrent()
        self.__fileList.clear()

        workspace = self.__workSpaceLabel.getWorkSpace()
        icon = QtGui.QIcon("map_pin.png")

        for fileName in fileList:
            item = QtWidgets.QListWidgetItem()

            if re.match(r'.+\..+', fileName) is not None:
                gpsPosition = ImageController.readGPS(f'{workspace}/{fileName}')

                if gpsPosition != '-':
                    item.setIcon(icon)

            item.setText(fileName)

            self.__fileList.addItem(item)

        if currentFileName in fileList:
            self.__fileList.setCurrentRow(fileList.index(currentFileName))

    def __convertToExifToolOptions(self, latlon):
        try:
            latlon = list(map(float, latlon.split(",")))
            if latlon[0] > 0:
                latitudeRef = "North"
            else:
                latitudeRef = "South"

            if latlon[1] > 0:
                longitudeRef = "East"
            else:
                longitudeRef = "West"

            option = ["-GPSLatitude=" + str(latlon[0]),
                      "-GPSLatitudeRef=" + latitudeRef,
                      "-GPSLongitude=" + str(latlon[1]),
                      "-GPSLongitudeRef=" + longitudeRef]
            return option
        except:
            print("Wrong type, Latitude or/and Longitude")
            return None

    def __implantGPSTag(self, latlon):
        ws = self.__workSpaceLabel.getWorkSpace()
        gpstag = self.__convertToExifToolOptions(latlon)

        if ws is None:
            return

        if gpstag is None:
            return

        for item in self.__fileList.selectedItems():
            fileName = item.text()

            cmd = ["exiftool"]
            cmd.extend(gpstag)
            cmd.append(f'{ws}/{fileName}')
            print(Foundation.run(cmd))

    def preprocessImplantGPSTag(self):
        # Get Latitude and Longitude from webView
        self.__webView.page().runJavaScript("\"\" + latitude + \",\" + longitude", self.__implantGPSTag)
